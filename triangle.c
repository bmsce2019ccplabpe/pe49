#include<stdio.h>
#include<math.h>
float
input ()
{
  float a;
  printf ("Enter the side of the triangle\n");
  scanf ("%f", &a);
  return a;
}



float
compute (float x, float y, float z)
{
  float s, area;
  s = (x + y + z) / 2;
  area = sqrt (s * (s - x) * (s - y) * (s - z));
  return area;
}

void
output (float area)
{
  printf ("The area of the triangle is %f\n", area);
}

int
main ()
{
  float a, b, c, ar;
  a = input ();
  b = input ();
  c = input ();
  ar = compute (a, b, c);
  output (ar);
  return 0;
}
