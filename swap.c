 #include<stdio.h>
void swap(int *num1,int *num2);
int main()
{
    int num1,num2;
    printf("enter two numbers\n");
    scanf("%d%d",&num1,&num2);
    printf("The value of variables before swap %d,%d\n",num1,num2);
    swap(&num1,&num2);
    printf("The value of variables after swap %d,%d\n",num1,num2);
}
void swap(int *num1,int *num2)
{
    int temp;
    temp=*num1;
    *num1=*num2;
    *num2=temp;
}